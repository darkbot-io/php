FROM php:8.3.17-fpm

ENV TIMEZONE UTC
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get clean
RUN apt-get update && apt-get install -y --install-recommends \
    apt-utils \
    libicu-dev \
    libzip-dev \
    libsodium-dev \
    libpng-dev \
    libmagickwand-dev \
    librabbitmq-dev \
    htop \
    procps \
    openssl \
    mariadb-client \
    redis-tools \
    vim \
    zip \
    unzip \
    iputils-ping \
    msmtp \
    mailutils \
    ntpdate \
    supervisor

RUN docker-php-ext-configure intl

RUN docker-php-ext-install \
    sockets \
    sysvsem \
    bcmath \
    intl \
    zip \
    pdo \
    pdo_mysql \
    mysqli \
    gd \
    pcntl \
    sodium

# Install mcrypt extension
#RUN apt-get install -y libmcrypt-dev && pecl install mcrypt-1.0.3 && docker-php-ext-enable mcrypt

RUN pecl install apcu && docker-php-ext-enable apcu
RUN pecl channel-update pecl.php.net

# Install imagick
# TODO: RUN pecl install imagick && docker-php-ext-enable imagick
# RUN set -eux; install-php-extensions Imagick/imagick@master

# Install amqp extension
RUN pecl install amqp && docker-php-ext-enable amqp
    
# Install Redis extension
RUN pecl install -o -f redis && rm -rf /tmp/pear && docker-php-ext-enable redis

# Install trader and imagick extension
RUN pecl install trader && docker-php-ext-enable trader

# Permission fix
# RUN usermod -u 1000 www-data

# Install git
RUN apt-get update && apt-get upgrade -y && apt-get install -y git

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer --version

# Set timezone
RUN ln -snf /usr/share/zoneinfo/${TIMEZONE} /etc/localtime && echo ${TIMEZONE} > /etc/timezone
RUN printf '[PHP]\ndate.timezone = "%s"\n', ${TIMEZONE} > /usr/local/etc/php/conf.d/tzone.ini
RUN "date"

COPY conf.d/php.ini /usr/local/etc/php/conf.d/php.ini
COPY conf.d/opcache.ini /usr/local/etc/php/conf.d/opcache.ini

EXPOSE 9000

WORKDIR /var/www/darkbot/api
